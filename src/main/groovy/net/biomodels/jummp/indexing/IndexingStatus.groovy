/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

/**
 * @short The states of an indexing plan can have.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

public enum  IndexingStatus {
    /**
     * The indexing plan is running to generate XML file(s).
     */
    GENERATING,
    /**
     * The generating XML file(s) is successful.
     */
    GENERATION_OK,
    /**
     * The generating XML file(s) is failed.
     */
    GENERATION_FAILED,
    /**
     * The verification of generating XML file(s) is performing.
     */
    VERIFYING,
    /**
     * The verification is good.
     */
    VERIFICATION_OK,
    /**
     * The verification is failed.
     */
    VERIFICATION_FAILED
}
