/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */





package net.biomodels.jummp.model

import grails.persistence.Entity

/**
 * @short This class handles model flags.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

@Entity
class Flag implements Serializable {
    /**
     * The textual representation of the flag
     */
    String label
    /**
     * The detailed explanation of what the flag is
     */
    String description
    /**
     * The graphical representation of the flag
     */
    byte[] icon

    static constraints = {
        label nullable: false, blank: false, unique: true
        description nullable: false, blank: false, maxSize: 1024
        icon nullable: false, maxSize: 3072 // rough max size of an icon of 32x32 px
    }

    static hasMany = [models: ModelFlag]
}
