/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */





package net.biomodels.jummp.model

import grails.persistence.Entity
import net.biomodels.jummp.annotationstore.ElementAnnotation

/**
 * @short This class handles types of model element.
 *
 * @author  Tung Nguyen <tung.nguyen@ebi.ac.uk>
 * @author  Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @date    15/12/2016
 */

@Entity
public class ModelElementType  implements Serializable {
    /**
     * A human readable name.
     * For instance, species, reaction, compartment, structuralModel, observationModel, etc.
     */
    String name
    /**
     * The name of a given ModelElementType instance should be belonged to a specific model format.
     * For instance:
     * SBML format typically has species, reaction, compartment, etc.
     * PharmML format basically goes with some element types such as structuralModel, observationModel, etc.
     */
    ModelFormat modelFormat

    static hasMany = [elementAnnotations: ElementAnnotation]

    static constraints = {
        name(nullable: false, blank: false)
        modelFormat(nullable: false, blank: false)
    }
}
