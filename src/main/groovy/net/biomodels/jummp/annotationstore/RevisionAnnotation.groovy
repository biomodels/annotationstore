/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.annotationstore

import grails.gorm.DetachedCriteria
import grails.persistence.Entity
import net.biomodels.jummp.model.Revision

/**
 * Class corresponding to the linker table between revision and element_annotation.
 *
 * We prefer explicitly defining this class over the standard many-to-many mapping
 * features provided by Hibernate/GORM because populating an entity's associated collection
 * prompts Hibernate to bump the version attribute of the entity. This has catastrophic
 * implications if you attempt to populate the association using multiple threads, because only
 * one thread succeeds and all the others fail with a StaleObjectStateException after the session
 * is flushed, hampering throughput.
 *
 * See http://burtbeckwith.com/blog/?p=169 and http://stackoverflow.com/a/28151520
 * for more information.
 */
@Entity
class RevisionAnnotation implements Serializable {
    ElementAnnotation elementAnnotation
    Revision revision

    static RevisionAnnotation get(long revisionId, long annotationId) {
        createCriteriaFor(revisionId, annotationId).get()
    }

    static boolean exists(long revisionId, long annotationId) {
        createCriteriaFor(revisionId, annotationId).count() > 0
    }

    static RevisionAnnotation create(Revision r, ElementAnnotation a, boolean flush = false) {
        def result = new RevisionAnnotation(revision: r, elementAnnotation: a)
        result.save(flush: flush, insert: true)
        result
    }

    private static DetachedCriteria createCriteriaFor(long revisionId, long annotationId) {
        RevisionAnnotation.where {
            revision == Revision.load(revisionId) &&
                    elementAnnotation == ElementAnnotation.load(annotationId)
        }
    }

    boolean equals(o) {
        if (!(o instanceof RevisionAnnotation)) {
            return false
        }

        elementAnnotation?.id == o?.elementAnnotation?.id && revision?.id == o?.revision?.id
    }

    int hashCode() {
        int result = (revision != null ? revision.hashCode() : 0)
        result = 31 * result + (elementAnnotation != null ? elementAnnotation.hashCode() : 0)
    }

    static mapping = {
        id composite: ['revision', 'elementAnnotation']
        version false
        revision column: 'revision_id'
        elementAnnotation column: 'element_annotation_id'
        table 'revision_annotations'
    }

    static constraints = {
        elementAnnotation validator: { ElementAnnotation anno, RevisionAnnotation association ->
            if (association.revision == null || association.revision.id == null) return
            boolean haveDuplicate = false
            RevisionAnnotation.withNewSession {
                haveDuplicate = RevisionAnnotation.exists(association.revision.id, anno.id)
            }
            return !haveDuplicate
        }
    }
}
