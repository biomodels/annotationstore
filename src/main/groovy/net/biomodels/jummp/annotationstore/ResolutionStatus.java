package net.biomodels.jummp.annotationstore;

/**
 * @author carankalle
 */

public enum ResolutionStatus {

    /*The annotation is resolved*/
    RESOLVED,

    /*This is default state which says the annotation is unresolved*/
    UNRESOLVED,

    /*Network/IO Error while resolving annotation*/
    RESOLVER_ERROR,

    /*No record is available for the annotation*/
    UNRESOLVABLE
}